function initApp() {
    // Login with Email/Password
    var logo = document.getElementById('signinLogo');
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnApple = document.getElementById('btnApple');
    var btnSignUp = document.getElementById('btnSignUp');
    
    logo.addEventListener('click', function() {
        window.location.href = "index.html";
    });

    btnLogin.addEventListener('click', function() {
        firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value).then(function(result) {
            window.location.href = "index.html";
        }).catch(function(error) {
            txtEmail.value = "";
            txtPassword.value = "";
            create_alert("error", error);
        });
    });

    btnGoogle.addEventListener('click', function() {
        var provider = new firebase.auth.GoogleAuthProvider();
        // provider.addScope('https://www.googleapis.com/auth/contacts.readonly');
        firebase.auth().signInWithPopup(provider).then(function(result) {

            var token = result.credential.accessToken;
            var user = result.user;
            window.location.href = "index.html";
        }).catch(function(error) {

            create_alert('error', error.message);
        });
    });

    btnApple.addEventListener('click', function() {
        var provider = new firebase.auth.OAuthProvider('apple.com');
        // provider.addScope('email');
        // provider.addScope('name');
        firebase.auth().signInWithPopup(provider).then(function(result) {

            var token = result.credential.accessToken;
            var user = result.user;
            window.location.href = "index.html";
        }).catch(function(error) {

            create_alert('error', error.message);
        });


    });

    btnSignUp.addEventListener('click', function() {
        console.log(txtEmail.value)
        console.log(txtPassword.value)
        firebase.auth().createUserWithEmailAndPassword(txtEmail.value, txtPassword.value).then(function(result) {
            txtEmail.value = "";
            txtPassword.value = "";
            create_alert("success", result);
        }).catch(function(error) {
            txtEmail.value = "";
            txtPassword.value = "";
            create_alert("error", error);
        });

    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function() {
    initApp();
};