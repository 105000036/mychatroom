# Software Studio 2021 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : [xxxx]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|N|
|Firebase Page|5%|N|
|Database|15%|N|
|RWD|15%|N|
|Topic Key Function|20%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|

# 作品網址：[xxxx]

## Website Detail Description
[xxxx]


# Components Description : 
1. [xxxx] : [xxxx]
2. [xxxx] : [xxxx]
3. [xxxx] : [xxxx]

# Other Functions Description : 
1. [xxxx] : [xxxx]
2. [xxxx] : [xxxx]
3. [xxxx] : [xxxx]

